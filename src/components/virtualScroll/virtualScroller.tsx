import React, { useState } from 'react'
import InfiniteScroll from "react-infinite-scroll-component";
import './virtualScroll.css'

export function VirtualScroller() {
    const [items, setItems] = useState(Array.from({ length: 20 }));

    const fetchMoreData = () => {
        setTimeout(() => {
            setItems(
                items.concat(Array.from({ length: 20 }))
            );
        }, 1500);
    };

    return (
        <div className='wrapper'>
            <InfiniteScroll
                dataLength={items.length}
                next={fetchMoreData}
                hasMore={true}
                loader={<h4>Loading...</h4>}
            >
                {items.map((i, index) => (
                      <div className='child' key={index}>
                            wadsdddddddddddddddasddddddddddddddddddddddddddddddddddddddddddddddddddsfdsfsdsdfsd
                      </div>
                ))}
            </InfiniteScroll>
        </div>
    )
}
