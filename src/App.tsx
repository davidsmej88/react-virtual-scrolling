import React from 'react';
import { VirtualScroller } from "./components/virtualScroll/virtualScroller";
import { InfiniteListWithVerticalScroll } from "./components/virtualScroll/virtualScrollHook";

function App() {
  return (
    <div>
        <VirtualScroller />
    </div>
  );
}

export default App;
